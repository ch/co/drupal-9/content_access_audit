<?php

/**
 * @file
 * Hooks for content_access_audit module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\user\Entity\Role;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function content_access_audit_form_content_access_page_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form_state->set('original_form', $form['per_role']);
  $form['#submit'][] = '_content_access_audit_log_changes';

  if (array_key_exists('reset', $form)) {
    $form['reset']['#submit'][] = '_content_access_audit_log_reset';
  }

  $form['content_access_audit'] = [
    '#type' => 'fieldset',
    '#title' => t('View content access audit log'),
  ];

  $node = $form_state->get('node');
  $nid = $node->id();

  $global_report_route = Url::fromRoute('content_access_audit.global_report');
  $global_report_link = Link::fromTextAndUrl('View the audit log of all changes to the content access settings for this site', $global_report_route);

  $node_report_route = Url::fromRoute('content_access_audit.node_report', ['nid' => $nid]);
  $node_report_link = Link::fromTextAndUrl('View the audit log of all changes to the content access settings for this node', $node_report_route);

  $links[] = $node_report_link;
  $links[] = $global_report_link;

  $form['content_access_audit']['links'] = [
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => $links,
  ];
}

/**
 * Log changes made via content_access node forms.
 *
 * @param array $form
 *   The form being altered.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 */
function _content_access_audit_log_changes(&$form, $form_state) {
  $node = $form_state->get('node');
  $nid = $node->id();

  $roles = array_keys(Role::loadMultiple());

  $before = [];
  $after = [];
  $changes = [];

  $ops['view'] = 'View any page content';
  $ops['view_own'] = 'View own page content';
  $ops['update'] = 'Edit any page content';
  $ops['update_own'] = 'Edit own page content';
  $ops['delete'] = 'Delete any page content';
  $ops['delete_own'] = 'Delete own page content';

  foreach ($ops as $op => $title) {
    foreach ($roles as $role) {
      if (in_array($role, $form_state->get('original_form')[$op]['#default_value'])) {
        $before[$op][] = $role;
      }

      if ($form_state->getValues()[$op][$role]) {
        $after[$op][] = $role;
      }

    }
  }

  foreach ($ops as $op => $title) {
    $removed = array_diff($before[$op], $after[$op]);
    $added = array_diff($after[$op], $before[$op]);
    if ($removed) {
      $changes[] = "The <em>$title</em> permission has been removed from: <em>" . implode(', ', $removed) . "</em>";
    }
    if ($added) {
      $changes[] = "The <em>$title</em> permission has been given to: <em>" . implode(', ', $added) . "</em>";
    }
  }

  if ($changes) {
    $msg = "The following permissions changes have been made for " . $node->getTitle() . ":<ul>";
    foreach ($changes as $change) {
      $row = [
        'timestamp' => time(),
        'uid' => \Drupal::currentUser()->id(),
        'nid' => $nid,
        'message' => $change,
      ];
      \Drupal::service('database')->insert('content_access_audit')->fields($row)->execute();
      $msg .= "<li>" . $change . "</li>";
    }
    $msg .= "</ul>";

    $messenger = \Drupal::messenger();
    $messenger->addMessage(Markup::create($msg));

  }
}

/**
 * Log changes made via content_access node forms.
 *
 * @param array $form
 *   The form being altered.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 */
function _content_access_audit_log_reset(&$form, $form_state) {
  $node = $form_state->get('node');
  $nid = $node->id();

  $msg = "Permissions have been reset to default.";
  $row = [
    'timestamp' => time(),
    'uid' => \Drupal::currentUser()->id(),
    'nid' => $nid,
    'message' => $msg,
  ];

  \Drupal::service('database')->insert('content_access_audit')->fields($row)->execute();
}
