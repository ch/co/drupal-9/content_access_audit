<?php

namespace Drupal\Tests\content_access_audit\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Check http statuses of report pages.
 *
 * @group content_access_audit
 */
class HttpStatusTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'content_access', 'content_access_audit'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a 'page' content type.
    $this->drupalCreateContentType(['type' => 'page']);

    $node = $this->drupalCreateNode(['type' => 'page']);

  }

  /**
   * Data provider.
   */
  public function httpResponseCodeProvider() {
    yield 'Node report status for extant node' => ['/node/1/content_access_audit', 200];

    // 999 => nid that does not exist.
    yield 'Node report status for absent node' => ['/node/999/content_access_audit', 404];

  }

  /**
   * Check HTTP return codes.
   *
   * @dataProvider httpResponseCodeProvider()
   */
  public function testHttpResponseCodes(string $node_report_url, int $expected_response) {
    $user = $this->drupalCreateUser(['grant content access']);
    $this->drupalLogin($user);

    $this->drupalGet($node_report_url);
    $this->assertSession()->statusCodeEquals($expected_response);
  }

}
