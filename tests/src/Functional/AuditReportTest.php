<?php

namespace Drupal\Tests\content_access\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Check we audit node permission changes correctly.
 *
 * @group content_access_audit
 */
class AuditReportTest extends BrowserTestBase {

  // Helper trait provided by the content_access module.
  use ContentAccessTestHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['content_access', 'content_access_audit'];

  /**
   * A user with admin rights.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Content type for test.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $contentType;

  /**
   * Node object to perform test.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $node1;

  /**
   * Node object to perform test.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $node2;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'grant content access',
      'bypass node access',
    ]);

    $this->contentType = $this->drupalCreateContentType();

    // Create test nodes.
    $this->node1 = $this->drupalCreateNode([
      'type' => $this->contentType->id(),
    ]);

    $this->node2 = $this->drupalCreateNode([
      'type' => $this->contentType->id(),
    ]);

    node_access_rebuild();

    $this->drupalLogin($this->adminUser);
    $this->changeAccessPerNode();

  }

  /**
   * Test that making a permission change is properly logged.
   */
  public function testPermissionChangeAudit() {
    // Restrict access to the content type.
    $accessPermissions = [
      'view[anonymous]' => FALSE,
    ];

    $this->changeAccessNode($this->node1, $accessPermissions);

    // Look for message as logged by _content_access_audit_log_changes().
    $this->drupalGet('node/' . $this->node1->id() . '/content_access_audit');
    $this->assertSession()->pageTextContains('permission has been removed from');

    // Expect no changes for node2.
    $this->drupalGet('node/' . $this->node2->id() . '/content_access_audit');
    $this->assertSession()->pageTextContains('No Content Access changes found for');

    // Expect node1 title in the global report...
    $this->drupalGet('admin/reports/content_access_audit');
    $this->assertSession()->pageTextContainsOnce($this->node1->getTitle());

    // ...and expect node2 title to be absent
    $this->assertSession()->responseNotContains($this->node2->getTitle());
  }

}
