<?php

namespace Drupal\content_access_audit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Produce reports of content_access changes.
 */
class ContentAccessAuditReportController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Entity manager service..
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * GlobalReport constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Database\Connection $dateFormatter
   *   The date formatter service..
   * @param \Drupal\Core\Database\Connection $entityManager
   *   The entity manager..
   */
  public function __construct(Connection $database, DateFormatter $dateFormatter, EntityTypeManagerInterface $entityManager) {
    $this->database = $database;
    $this->dateFormatter = $dateFormatter;
    $this->entityManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Page controller to produce global audit report.
   */
  public function globalReport() {
    $report['heading'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => 'Content Access audit',
    ];

    $header = ['Time', 'User', 'Page', 'Change'];

    $limit = 25;
    $query = $this->database->select('content_access_audit', 'a')
      ->fields('a', ['nid', 'uid', 'timestamp', 'message'])
      ->orderBy('timestamp', 'DESC')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit($limit);

    $results = $query->execute()->fetchAll();

    $rows = [];

    if (count($results) > 0) {
      foreach ($results as $result) {
        $node = $this->entityManager->getStorage('node')->load($result->nid);
        $title = $node->getTitle();
        $url = Url::fromRoute('entity.node.canonical', ['node' => $result->nid]);
        $link = Link::fromTextAndUrl($title, $url);

        $user = $this->entityManager->getStorage('user')->load($result->uid);

        $rows[] = [
          $this->dateFormatter->format($result->timestamp, 'short'),
          $user->getAccountname(),
          $link,
          Markup::create($result->message),
        ];
      }
    }

    $report['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
    $report[] = [
      '#type' => 'pager',
    ];

    return $report;
  }

  /**
   * Page controller to produce audit report for a node.
   */
  public function nodeReport(int $nid): array {

    $node = $this->entityManager->getStorage('node')->load($nid);

    if (!$node) {
      throw new NotFoundHttpException();
    }

    $title = $node->getTitle();

    $report['heading'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => Markup::Create("Content Access audit log for: <em>$title</em>"),
    ];

    $header = ['Time', 'User', 'Change'];

    $limit = 25;
    $query = $this->database->select('content_access_audit', 'a')
      ->fields('a', ['uid', 'timestamp', 'message'])
      ->condition('nid', $nid, '=')
      ->orderBy('timestamp', 'DESC')
      ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit($limit);

    $results = $query->execute()->fetchAll();

    $rows = [];

    if (count($results) > 0) {
      foreach ($results as $result) {
        $user = $this->entityManager->getStorage('user')->load($result->uid);

        $rows[] = [
          $this->dateFormatter->format($result->timestamp, 'short'),
          $user->getAccountname(),
          Markup::create($result->message),
        ];
      }
      $report['table'] = [
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      ];

      $report[] = [
        '#type' => 'pager',
      ];

    }
    else {
      $report['no_results'] = ['#markup' => 'No Content Access changes found for <em>' . $title . '</em>'];
    }
    return $report;
  }

}
